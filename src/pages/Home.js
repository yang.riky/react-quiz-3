import { useSelector } from "react-redux";
import { selectCount } from "../features/counter/counterSlice";

const Home = () => {
    const count = useSelector(selectCount);

    return <h1>Home (Counter Value: {count})</h1>;
};

export default Home;