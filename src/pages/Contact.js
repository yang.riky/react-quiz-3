import { useSelector } from "react-redux";
import { selectCount } from "../features/counter/counterSlice";

const Contact = () => {
    const count = useSelector(selectCount);

    return <h1>Contact Me (Counter Value: {count})</h1>;
};

export default Contact;