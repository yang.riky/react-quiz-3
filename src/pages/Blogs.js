import { useSelector } from "react-redux";
import { selectCount } from "../features/counter/counterSlice";

const Blogs = () => {
    const count = useSelector(selectCount);

    return <h1>Blog Articles (Counter Value: {count})</h1>;
};

export default Blogs;